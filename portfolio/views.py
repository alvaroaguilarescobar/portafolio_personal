from django.shortcuts import render
from .models import Project
from django.http import HttpResponse
import random

def home(request):
    projects = Project.objects.order_by('-id')[:3]
    tituloPagina = 'Portfolio - Todos mis proyectos'
    return render(request,'portfolio/home.html',{'projects':projects,'tituloPagina':tituloPagina})
