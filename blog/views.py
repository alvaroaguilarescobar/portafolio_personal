from django.shortcuts import render, get_object_or_404
from .models import Post

def all_blogs(request):
    cantPosts = Post.objects.count
    posts = Post.objects.order_by('-datePost')[:3]
    tituloPagina = ' Blog de Álvaro - Todos mis Posts'
    cantidadPosts = 3
    return render(request,'blog/all_blogs.html',{'posts':posts,'cantPosts':cantidadPosts,'tituloPagina':tituloPagina})

def detail(request,blog_id):
    blog = get_object_or_404(Post, pk=blog_id)
    tituloPagina = ' Blog de Álvaro - '+blog.titlePost
    return render(request,'blog/detail.html',{'blog':blog,'tituloPagina':tituloPagina})
