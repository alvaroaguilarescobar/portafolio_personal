from django.db import models

class Post(models.Model):
    titlePost = models.CharField(max_length=200)
    datePost = models.DateField()
    contentPost = models.TextField()

    def __str__(self):
        return self.titlePost
